import 'package:flutter/material.dart';

// * COMPONENT
// * ERCAN -> CİRCLE PROGRESS BURADAN GELMEKTEDİR.
class CircleProgressIndicatorComponent extends StatelessWidget {
  const CircleProgressIndicatorComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CircularProgressIndicator(
      backgroundColor: Colors.transparent,
      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
    );
  }
}
