import 'package:flutter/material.dart';
import 'colors/light_theme_colors.dart';
import 'style/elevated_button_style.dart';
import 'style/bottom_navbar_style.dart';

extension LightTheme on BuildContext {
  ThemeData get lightTheme => ThemeData(
        scaffoldBackgroundColor:
            LightThemeColor.instance.scaffoldBackgroundColor,
        elevatedButtonTheme: lightElevatedButtonStyle,
        bottomNavigationBarTheme: lightBottomNavbar,
        colorScheme: const ColorScheme.light(),
      );
}
