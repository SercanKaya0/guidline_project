import 'package:flutter/material.dart';

class LightThemeColor {
  static final LightThemeColor instance = LightThemeColor();
  final Color scaffoldBackgroundColor = const Color(0XFFe9ecf6);
}
