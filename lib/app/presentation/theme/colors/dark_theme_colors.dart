import 'package:flutter/material.dart';

class DarkThemeColor {
  static final DarkThemeColor instance = DarkThemeColor();
  final Color scaffoldBackgroundColor = const Color(0XFF000b21);
  // Git commit algılasın diye eklendi işlevi yok silinekece.
  final Color elevatedButton = Colors.black;
}
