import 'package:flutter/material.dart';

// * Extension
// * Button Style
// * Ercan -> Elevated button Style buradan ayarlanmaktadır. 2 tema içinde.
extension ElevatedButtonStyleExtension on BuildContext {
  ElevatedButtonThemeData get darkElevatedButtonStyle =>
      ElevatedButtonThemeData(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
              foregroundColor: MaterialStateProperty.all<Color>(Colors.black)));

  ElevatedButtonThemeData get lightElevatedButtonStyle =>
      ElevatedButtonThemeData(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white)));
}
