import 'package:flutter/material.dart';
import 'package:guidline_project/core/theme/theme_helper.dart';

// * TextStyle
// * Extension
// * Ercan => patuaOneTextStyle buradan gelir, Buradan dilediğin customize işlemlerini yapabilrsin.
extension TextStyleExtension on BuildContext {
  TextStyle patuaOneTextStyle(
          {double? fontSize, Color? isDark, Color? isLight}) =>
      isThemeControl(
        this,
        isDark: TextStyle(
          fontFamily: "PatuaOne",
          fontSize: fontSize ?? 30,
          color: isDark ?? Colors.white,
        ),
        isLight: TextStyle(
          fontFamily: "PatuaOne",
          fontSize: fontSize ?? 30,
          color: isLight ?? Colors.black,
        ),
      );
  TextStyle josefinSansTextStyle(
          {double? fontSize, Color? isDark, Color? isLight}) =>
      isThemeControl(
        this,
        isDark: TextStyle(
            fontFamily: "JosefinSans",
            fontSize: fontSize ?? 16,
            color: isDark ?? Colors.grey,
            fontWeight: FontWeight.bold),
        isLight: TextStyle(
            fontFamily: "JosefinSans",
            fontSize: fontSize ?? 16,
            color: isLight ?? Colors.grey,
            fontWeight: FontWeight.bold),
      );
  TextStyle latoTextStyle({double? fontSize, Color? isDark, Color? isLight}) =>
      isThemeControl(
        this,
        isDark: TextStyle(
          fontFamily: "Lato",
          fontSize: fontSize ?? 30,
          color: isDark ?? Colors.white,
        ),
        isLight: TextStyle(
          fontFamily: "Lato",
          fontSize: fontSize ?? 30,
          color: isLight ?? Colors.white,
        ),
      );
}
