import 'package:flutter/material.dart';
import 'package:guidline_project/core/theme/theme_helper.dart';


extension CupertinoStyleExtension on BuildContext {
  Color get cupertinoBackgroundColor => isThemeControl(this, isDark: Colors.black26, isLight: Colors.white30);

}
