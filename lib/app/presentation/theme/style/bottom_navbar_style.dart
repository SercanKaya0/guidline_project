import 'package:flutter/material.dart';

import '../../../../core/hex_color/hex_color_helper.dart';

// * BottomNavBar
// * Ercan-> Bottom navbar default ayarlar buradan gelmektedir.
extension BottomNavbarStyle on BuildContext {
  BottomNavigationBarThemeData get darkBottomNavbar =>
      const BottomNavigationBarThemeData(
        backgroundColor: Color(0XFF192438),
        unselectedItemColor: Colors.grey,
        selectedItemColor: Colors.white,
      );

  BottomNavigationBarThemeData get lightBottomNavbar =>
      BottomNavigationBarThemeData(
        backgroundColor: Colors.white,
        unselectedItemColor: const Color(0XFF000b21),
        selectedItemColor: HexColor.fromHex("#0057FF"),
      );
}
