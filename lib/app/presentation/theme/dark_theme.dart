import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'colors/dark_theme_colors.dart';
import 'style/bottom_navbar_style.dart';
import 'style/elevated_button_style.dart';

extension DarkTheme on BuildContext {
  ThemeData get darkTheme => ThemeData(
        scaffoldBackgroundColor:
            DarkThemeColor.instance.scaffoldBackgroundColor,
        elevatedButtonTheme: darkElevatedButtonStyle,
        colorScheme: const ColorScheme.dark(),
        bottomNavigationBarTheme: darkBottomNavbar,
      );
}
