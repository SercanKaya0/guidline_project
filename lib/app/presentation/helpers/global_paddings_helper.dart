// * GLOBAL
// * PADDING
// * Extension
// * ERCAN -> UYGULAMADA KULLANILAN PADDİNGLERİ BURADAN AYARLIYORUM.

import 'package:flutter/material.dart';

extension GlobalPaddingExtension on BuildContext {
  EdgeInsets get globalHorizontal24x =>
      const EdgeInsets.symmetric(horizontal: 24);
  EdgeInsets get globalHorizontal16x =>
      const EdgeInsets.symmetric(horizontal: 8);
}
