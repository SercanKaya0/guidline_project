import 'package:flutter/material.dart';


// * Extension
// * Ercan -> HexColor direk çalıştıran method.
extension HexColor on Color {
 static Color fromHex(String hextString) {
    final buffer = StringBuffer();
    if (hextString.length == 6 || hextString.length == 7) buffer.write('ff');
    buffer.write(hextString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
