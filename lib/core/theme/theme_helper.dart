import 'package:flutter/material.dart';

extension ThemeControllerExtension on BuildContext {
  T isThemeControl<T>(BuildContext context,
      {required T isDark, required T isLight}) {
    return Theme.of(context).colorScheme == const ColorScheme.dark()
        ? isDark
        : isLight;
  }
}
