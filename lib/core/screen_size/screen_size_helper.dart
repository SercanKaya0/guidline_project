import 'package:flutter/material.dart';

extension ScreenSizeExtension on BuildContext {
  double screenWidht(BuildContext context,{double? width}) => MediaQuery.of(context).size.width * (width ?? 1);
  double screenHeight(BuildContext context,{double? height}) => MediaQuery.of(context).size.height * (height ?? 1);

}
