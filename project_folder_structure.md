## Project Folder Structure

 - [App](#app) </br>
    - [Data](#data) </br>
        - [Local]($data) </br>
             - [Model]($data) </br>
             - [Services](#data) </br>
        - [Remote](#data) </br>
            - [Clients](#clients) </br>
            - [Model](#data) </br>
            - [Services](#data) </br>
                 - [XScreenServices](#data) </br>
                 - [YScreenServices](#data) </br>
                 - [ZScreenServices](#data) </br>
    - [Navigation](#navigation) </br>
    - [Presentation](#presentation) </br>
         - [Components](#components) </br>
         - [GlobalHelpers](#globalhelper) </br>
         - [Screens](#presentation) </br>
             - [Screens](#presentation) </br>
             - [Widgets](#presentation) </br>
             - [ViewModel](#presentation) </br>
             - [Repository](#repository) </br>
         - [Theme](#theme) </br> 
                 - [Styles](#theme) </br>
                 - [Colors](#theme) </br>
 - [Core](#core) </br>
 - [Configrue](#configrue) </br>


 # Project Folder Tree
```
├── ProjectFolder

  ├── Data
    ├── Local
        ├── Model
            ├── onboard_model.dart
            ├── x_page_model.dart
        ├── Services
            ├── onboard_screen_datasource.dart
            ├── onobard_screen_services.dart
    ├── Remote
        ├── Model
            ├── user_login.dart
            ├── x_page_model.dart
        ├── Services
            ├── user_login_datasource.dart
            ├── user_login_services.dart

  ├── Configrue
    ├── x_belediye.env
    ├── y_belediye.env
    ├── z_belediye.env

  ├── Navigation
    ├── navigation.dart

  ├── Presentation
    ├── Screens
        ├── Home
        ├── Login
        ├── Register
        ├── PasswordForgot
    ├── Components
        ├── appar_components.dart
    ├── GlobalHelpers
        ├── global_padding_extension_helper.dart
    ├── Theme
        ├── dark_theme_extension.dart
        ├── light_theme_extension.dart
    ├── Styles
        ├── ButtonStyle
            ├── elevated_button_style.dart
            ├── float_action_button_style.dart
        ├── TextStyle
    
````
# PROJECT

<a name="app"></a>
## App
<details>
Tüm uygulama buradan ayağa kalkmalıdır. Tüm ekranlar ve diğer core olmayan işlemler bu klasör altından ayarlanmalıdır.

```text
├── Data
├── Navigation
├── Presentation
├── ...
```
</details>

<a name="repository"></a>
## Repository
<details>
Ekrana gönderilecek veri burada depolanır. Buraya veriler Data katmanında ki datasourcedan alınır. Unutma tüm işlevler bir interface üzerinden gerçekleşir ve dependcy işlemleri ona göre yapılır.

```text
├── Repository
    ├── login_repository.dart
├── ...
```
</details>

<a name="globalhelper"></a>
## Global Helper
<details>
Uygulamada global olarak kullanılacak tüm yardımcı işlevler burada olmalıdır. örneğin tüm ekranlarda yada 1 den fazla ekranda kenardan boşluklar bırakılacak bu klasörde default olarak ayarlanmalıdır.

```text
├── Global Helper
    ├── global_padding_helper.dart
    ├── global_spacer_flex.dart
├── ...
```
</details>

<a name="components"></a>
## Components
<details>
Eğer uygulama içerisin de global olarak bir component bir ekran var ise burada olmalıdır. Ekrana özel componentleri bu klasöre asla almıyoruz o bir component değil widget'tır. Örneğin AppBar bir componenttir. 

```text
├── Components
    ├── appbar_component.dart
    ├── loading_component.dart
├── ...
```
</details>


<a name="data"></a>
## Data
<details>
Uygulama data kaynağı bu klasörden ayarlanmalıdır. Eğer dışarıya bir http istek atılmıyorsa localde eğer dışarı bir istek gidiyor ise remote dosyasında işlemler yapılmalıdır. Uygulama data kaynağı buradan ayarlanmalıdır

```text
├── Data
    ├── Local
        ├── Services
            ├── Onboard
                ├── onboard_data_source.dart
                ├── onboard_services.dart
    ├── Remote
        ├── Services
                ├── login_data_source.dart
                ├── login_services.dart
├── ...
```
</details>

<a name="clients"></a>
## Clients
<details>
Uygulama BaseURL'ini buradan almaktadır. Eğer base url apisi tüm uygulamada aynı ise bir adet client oluşturup tüm istekler bu client üzerinden gönderilmelidir. Eğer Base URL birden fazla ise farklı clientleri bu klasörde oluşturulup kullanılmalıdır.

```text
├── Client
    ├── base_client_v1.dart
    ├── base_client_v2.dart
├── ...
```
</details>

<a name="theme"></a>
## Theme
<details>
Uygulama içerisinde kullanılan tüm tema ayarlamaları buradan yapılmalıdır. Tüm default tema ayarı buradan config edilmedir. View içerisinde custom olarak bir default renk tanımı yapılmamlıdır.

```text
├── Theme
    ├── dark_theme.dart
    ├── light_theme.dart
├── ...
```
</details>

<a name="configrue"></a>
## Configrue
<details>
Diğer projelerde de kullanılabilecek core yapıları bu klasörde toplantı, Asla sadece bu projede kullanılacak bir core yapısı bu klasör içerisinde olmamalıdır.

```text
├── ScreenSize
├── ResponseView
├── ...
```
</details>

<a name="Core"></a>
## Core
<details>
Belediyelere özel ayarlamalar bu klasör altında toplanmalıdır. Bir .xbelediye.env dosyası altından erişim sağlanmalıdır. 

```text
├── esenler.env
├── beylikdüzü.env
├── ...
```
</details>

<a name="navigation"></a>
## Navigation
<details>
Uygulama içersinde ki tüm ekranlara ulaşım bu dosyadan olmalıdır. View içerisinde asla navigator.of diyerek bir navigation işlemi yapılmamalıdır. Bu yapıyı destekleyen yapı muhakkak core dosyasında olacaktır lütfen kontrol ediniz.

```text
class Navigation {
  static const home = "/";
  static const inAppScreen = "/in_app_screen";
  static const musicDownloadScreen = "/music_download_screen";
  static const searchScreen = "/searc_screen";
  static const settingScreen = "/settingScreen";

  static Route<dynamic> onGenerateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case home:
        return _materialPageRoute(AuthScreen());
      case inAppScreen:
        return _materialPageRoute(InAppScreen());
      case musicDownloadScreen:
        return _materialPageRoute(const MusicScreen());
      case searchScreen:
        return _materialPageRoute(const SearchScreen());
      case settingScreen:
        return _materialPageRoute(const SettingsScreen());
      default:
        return _materialPageRoute(const Text("PremiumPager"));
    }
  }

  static PageRouteBuilder<dynamic> _materialPageRoute(Widget page) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(0.0, 1.0);
        const end = Offset.zero;
        final tween = Tween(begin: begin, end: end);
        final offsetAnimation = animation.drive(tween);
        return SlideTransition(position: offsetAnimation, child: child);
      },
    );
  }
}
```
</details>


<a name="presentation"></a>
## Presentation
<details>
Uygulama içerisinde ki tüm ekranlar bu klasör altında olmalıdır. Screens klasörünün içerisine ekranları ekleyin. Aksi halde gönderilen Git PR isteği reddedilir.

```text
├── Screens
    ├── Home
        ├── home_screen.dart
    ├── Login
        ├── login_screen.dart
    ├── ...
```
</details>


