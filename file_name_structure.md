# Project File Structure

- Class isimleri Kesinlikle Büyük Harf ile Başlamalı ve CamelCase yazım kuralına uygun olmalıdır.
```
class ExampleClass {}
```
- File isimleri tamamı küçük harf olmalı ve türkçe karakter kullanılmamalıdır.
```
home_screen.dart
```
- İsimlendirmeler ingilizce olmalıdır. 
```
anasayfa.dart
class Anasayfa {}

Bu isimlendirmeler hatalı olarak değerlendirilecektir.
```